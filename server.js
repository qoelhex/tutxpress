
var express = require('express');
var app = express(); // create our app w/ express
var mongoose = require('mongoose'); // mongoose for mongodb
var morgan = require('morgan'); // log requests to the console (express4)
var bodyParser = require('body-parser'); // pull information from HTML POST
											// (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT
													// (express4)

mongoose.connect(process.env.OPENSHIFT_MONGODB_DB_URL
		|| 'mongodb://root:maneh666@localhost/local');
mongoose.connect();

app.use(express.static(__dirname + '/public')); // set the static files location
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({
	'extended' : 'true'
})); 
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({
	type : 'application/vnd.api+json'
})); 
app.use(methodOverride());

app.listen(process.env.OPENSHIFT_NODEJS_PORT || 3000);
